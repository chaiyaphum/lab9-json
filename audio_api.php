<?php

$arrayContent =  array ( "audio"  => array (
	array (
		"title" => "มาร์ช ม.ข",       
		"author" => array (
			"petition" => "พร พิรุณ",
			"melody" => "เอื้อ สุนทรสนาน"
			),
		"content" => "มหาวิทยาลัยขอนแก่นเกริกไกรวิทยา",  
		),
	array (
		"title" => "เสียงสนครวญ",       
		"author" => array (
			"petition" => "สุนทราภรณ์",
			"melody" => "เสุนทราภรณ์"
			),
		"content" => "เสียงยอดสนอ่อนโอนพลิ้วโยนยอดไหว",  
		)
	)
);

echo utf8_encode(json_encode($arrayContent));

?>